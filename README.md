# Check_mk plugin webchecks

GPL (C) Lars Falk-Petersen <dev@falk-petersen.no>, 2018

## Overview

Check_mk plugin to run various automated and manual web checks.

* Automatically reads apache, nginx configs and use info for checks.
* Checks http status code
* Checks SSL age (different threshold for Let's encrypt and normal SSL)
* Checks if URL resolves to target server, exception list for web proxies.
* String check (manual)

## Check_mk parameters

Replace webchecks_default_values with i.e. (10, 11, 12) for http timeout, ssl critical days and ssl warning days.

## Requirements

* On check_mk server
  * holidays (```python -m pip install -r requirements.txt``` or ```sudo apt install python3-holidays```)

## Files

* webchecks-agent.py
  * Find URLs, strings and status codes to check in a file on target
  * Place on target: `/usr/lib/check_mk_agent/plugins/`
* webchecksconfig.py
  * Config for webchecks-agent.py
  * Place on target: `/local/etc/`
* webchecks
  * Read output from agent, and run checks
  * Place on surveillance server: `local/share/check_mk/checks/`
  * Check that plugin is recognized: `check_mk -L | grep webcheck`

## Example

Output from agent:

```bash
$ ./webchecks-agent.py
<<<webchecks>>>
http://example.no/ 200 string to look for
```

## Agent config

Agent will pick up config from file webchecksconfig.py. Example config:

```python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

CONFIG = [
    {
        'url': 'http://bomlo-nytt.no/nyhende',
        'status': 200,
        'string': 'kultur',
    },
    {
        'url': 'https://example.no/',
        'string': 'tailored applications',
    },
    {
        'url': 'https://expired.badssl.com/',
    },
]
```

Example output:

```bash
OMD[omdproxy01]:~$ cmk --debug -v example.no|grep -i webchecks
webchecks http://example.no:80/ OK - URL resolves to correct host. HTTP OK (200). 
webchecks https://example.no/ OK - URL resolves to correct host. HTTP OK (200). Automated SSL expires in 84 days (warn 10, crit 5).
```

## TODO

* Inverse string check?
* Don't autocheck urls that are also manually configured
* Die nicely if config file is invalid.
* Change config file format? If user has access to edit config file, they can run any command as root.
* Seems lots of variables are available in compiled check var/check_mk/precompiled/example.no.py (Check_mk API), like ipaddress and exit status codes. Change to use those.
* check_mk says not to import datetime, only time.
* Make status code (200) default to no check (via -1 or 0)

## External code

python-nginx from https://github.com/peakwinter/python-nginx is pasted in webchecks-agent
