#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Check_mk agent for target defined web checks
# GPL (C) Lars Falk-Petersen <dev@falk-petersen.no>, 2018
#
# Find URLs, strings and status codes to check in a file on target

import subprocess  # To run nginx / apachectl
import socket  # Hostname
import sys  # Import config
import os  # Import config, import nginx config
import glob  # Searching for nginx configs
import argparse

config_file = "/local/etc/webchecksconfig.py"

'''
python-nginx from https://github.com/peakwinter/python-nginx:
'''

"""
Python library for editing NGINX serverblocks.

python-nginx
(c) 2016 Jacob Cook
Licensed under GPLv3, see LICENSE.md
"""

import re

INDENT = '    '


def bump_child_depth(obj, depth):
    children = getattr(obj, 'children', [])
    for child in children:
        child._depth = depth + 1
        bump_child_depth(child, child._depth)


class Conf(object):
    """
    Represents an nginx configuration.

    A `Conf` can consist of any number of server blocks, as well as Upstream
    and other types of containers. It can also include top-level comments.
    """

    def __init__(self, *args):
        """
        Initialize object.

        :param *args: Any objects to include in this Conf.
        """
        self.children = list(args)

    def add(self, *args):
        """
        Add object(s) to the Conf.

        :param *args: Any objects to add to the Conf.
        :returns: full list of Conf's child objects
        """
        self.children.extend(args)
        return self.children

    def remove(self, *args):
        """
        Remove object(s) from the Conf.

        :param *args: Any objects to remove from the Conf.
        :returns: full list of Conf's child objects
        """
        for x in args:
            self.children.remove(x)
        return self.children

    def filter(self, btype='', name=''):
        """
        Return child object(s) of this Conf that satisfy certain criteria.

        :param str btype: Type of object to filter by (e.g. 'Key')
        :param str name: Name of key OR container value to filter by
        :returns: full list of matching child objects
        """
        filtered = []
        for x in self.children:
            if name and isinstance(x, Key) and x.name == name:
                filtered.append(x)
            elif isinstance(x, Container) and x.__class__.__name__ == btype\
                    and x.value == name:
                filtered.append(x)
            elif not name and btype and x.__class__.__name__ == btype:
                filtered.append(x)
        return filtered

    @property
    def servers(self):
        """Return a list of child Server objects."""
        return [x for x in self.children if isinstance(x, Server)]

    @property
    def server(self):
        """Convenience property to fetch the first available server only."""
        return self.servers[0]

    @property
    def as_list(self):
        """Return all child objects in nested lists of strings."""
        return [x.as_list for x in self.children]

    @property
    def as_dict(self):
        """Return all child objects in nested dict."""
        return {'conf': [x.as_dict for x in self.children]}

    @property
    def as_strings(self):
        """Return the entire Conf as nginx config strings."""
        ret = []
        for x in self.children:
            if isinstance(x, (Key, Comment)):
                ret.append(x.as_strings)
            else:
                for y in x.as_strings:
                    ret.append(y)
        if ret:
            ret[-1] = re.sub('}\n+$', '}\n', ret[-1])
        return ret


class Container(object):
    """
    Represents a type of child block found in an nginx config.

    Intended to be subclassed by various types of child blocks, like
    Locations or Geo blocks.
    """

    def __init__(self, value, *args):
        """
        Initialize object.

        :param str value: Value to be used in name (e.g. regex for Location)
        :param *args: Any objects to include in this Conf.
        """
        self.name = ''
        self.value = value
        self._depth = 0
        self.children = list(args)
        bump_child_depth(self, self._depth)

    def add(self, *args):
        """
        Add object(s) to the Container.

        :param *args: Any objects to add to the Container.
        :returns: full list of Container's child objects
        """
        self.children.extend(args)
        bump_child_depth(self, self._depth)
        return self.children

    def remove(self, *args):
        """
        Remove object(s) from the Container.

        :param *args: Any objects to remove from the Container.
        :returns: full list of Container's child objects
        """
        for x in args:
            self.children.remove(x)
        return self.children

    def filter(self, btype='', name=''):
        """
        Return child object(s) of this Server block that meet certain criteria.

        :param str btype: Type of object to filter by (e.g. 'Key')
        :param str name: Name of key OR container value to filter by
        :returns: full list of matching child objects
        """
        filtered = []
        for x in self.children:
            if name and isinstance(x, Key) and x.name == name:
                filtered.append(x)
            elif isinstance(x, Container) and x.__class__.__name__ == btype\
                    and x.value == name:
                filtered.append(x)
            elif not name and btype and x.__class__.__name__ == btype:
                filtered.append(x)
        return filtered

    @property
    def locations(self):
        """Return a list of child Location objects."""
        return [x for x in self.children if isinstance(x, Location)]

    @property
    def comments(self):
        """Return a list of child Comment objects."""
        return [x for x in self.children if isinstance(x, Comment)]

    @property
    def keys(self):
        """Return a list of child Key objects."""
        return [x for x in self.children if isinstance(x, Key)]

    @property
    def as_list(self):
        """Return all child objects in nested lists of strings."""
        return [self.name, self.value, [x.as_list for x in self.children]]

    @property
    def as_dict(self):
        """Return all child objects in nested dict."""
        dicts = [x.as_dict for x in self.children]
        return {'{0} {1}'.format(self.name, self.value): dicts}

    @property
    def as_strings(self):
        """Return the entire Container as nginx config strings."""
        ret = []
        container_title = (INDENT * self._depth)
        container_title += '{0}{1} {{\n'.format(
            self.name, (' {0}'.format(self.value) if self.value else '')
        )
        ret.append(container_title)
        for x in self.children:
            if isinstance(x, Key):
                ret.append(INDENT + x.as_strings)
            elif isinstance(x, Comment):
                if x.inline and len(ret) >= 1:
                    ret[-1] = ret[-1].rstrip('\n') + '  ' + x.as_strings
                else:
                    ret.append(INDENT + x.as_strings)
            elif isinstance(x, Container):
                y = x.as_strings
                ret.append('\n' + y[0])
                for z in y[1:]:
                    ret.append(INDENT + z)
            else:
                y = x.as_strings
                ret.append(INDENT + y)
        ret[-1] = re.sub('}\n+$', '}\n', ret[-1])
        ret.append('}\n\n')
        return ret


class Comment(object):
    """Represents a comment in an nginx config."""

    def __init__(self, comment, inline=False):
        """
        Initialize object.

        :param str comment: Value of the comment
        :param bool inline: This comment is on the same line as preceding item
        """
        self.comment = comment
        self.inline = inline

    @property
    def as_list(self):
        """Return comment as nested list of strings."""
        return [self.comment]

    @property
    def as_dict(self):
        """Return comment as dict."""
        return {'#': self.comment}

    @property
    def as_strings(self):
        """Return comment as nginx config string."""
        return '# {0}\n'.format(self.comment)


class Http(Container):
    """Container for HTTP sections in the main NGINX conf file."""

    def __init__(self, *args):
        """Initialize."""
        super(Http, self).__init__('', *args)
        self.name = 'http'


class Server(Container):
    """Container for server block configurations."""

    def __init__(self, *args):
        """Initialize."""
        super(Server, self).__init__('', *args)
        self.name = 'server'

    @property
    def as_dict(self):
        """Return all child objects in nested dict."""
        return {'server': [x.as_dict for x in self.children]}


class Location(Container):
    """Container for Location-based options."""

    def __init__(self, value, *args):
        """Initialize."""
        super(Location, self).__init__(value, *args)
        self.name = 'location'


class Events(Container):
    """Container for Event-based options."""

    def __init__(self, *args):
        """Initialize."""
        super(Events, self).__init__('', *args)
        self.name = 'events'


class LimitExcept(Container):
    """Container for specifying HTTP method restrictions."""

    def __init__(self, value, *args):
        """Initialize."""
        super(LimitExcept, self).__init__(value, *args)
        self.name = 'limit_except'


class Types(Container):
    """Container for MIME type mapping."""

    def __init__(self, value, *args):
        """Initialize."""
        super(Types, self).__init__(value, *args)
        self.name = 'types'


class If(Container):
    """Container for If conditionals."""

    def __init__(self, value, *args):
        """Initialize."""
        super(If, self).__init__(value, *args)
        self.name = 'if'


class Upstream(Container):
    """Container for upstream configuration (reverse proxy)."""

    def __init__(self, value, *args):
        """Initialize."""
        super(Upstream, self).__init__(value, *args)
        self.name = 'upstream'


class Geo(Container):
    """
    Container for geo module configuration.

    See docs here: http://nginx.org/en/docs/http/ngx_http_geo_module.html
    """

    def __init__(self, value, *args):
        """Initialize."""
        super(Geo, self).__init__(value, *args)
        self.name = 'geo'


class Map(Container):
    """Container for map configuration."""

    def __init__(self, value, *args):
        """Initialize."""
        super(Map, self).__init__(value, *args)
        self.name = 'map'


class Key(object):
    """Represents a simple key/value object found in an nginx config."""

    def __init__(self, name, value):
        """
        Initialize object.

        :param *args: Any objects to include in this Server block.
        """
        self.name = name
        self.value = value

    @property
    def as_list(self):
        """Return key as nested list of strings."""
        return [self.name, self.value]

    @property
    def as_dict(self):
        """Return key as dict key/value."""
        return {self.name: self.value}

    @property
    def as_strings(self):
        """Return key as nginx config string."""
        if self.value == '' or self.value is None:
            return '{0};\n'.format(self.name)
        if '"' not in self.value and (';' in self.value or '#' in self.value):
            return '{0} "{1}";\n'.format(self.name, self.value)
        return '{0} {1};\n'.format(self.name, self.value)


def loads(data, conf=True):
    """
    Load an nginx configuration from a provided string.

    :param str data: nginx configuration
    :param bool conf: Load object(s) into a Conf object?
    """
    f = Conf() if conf else []
    lopen = []
    index = 0

    while True:
        m = re.compile(r'^\s*events\s*{', re.S).search(data[index:])
        if m:
            e = Events()
            lopen.insert(0, e)
            index += m.end()
            continue

        m = re.compile(r'^\s*http\s*{', re.S).search(data[index:])
        if m:
            h = Http()
            lopen.insert(0, h)
            index += m.end()
            continue

        m = re.compile(r'^\s*server\s*{', re.S).search(data[index:])
        if m:
            s = Server()
            lopen.insert(0, s)
            index += m.end()
            continue

        m = re.compile(r'^\s*location\s*(.*?\S+)\s*{', re.S).search(data[index:])
        if m:
            l = Location(m.group(1))
            lopen.insert(0, l)
            index += m.end()
            continue

        m = re.compile(r'^\s*if\s*(.*?\S+)\s*{', re.S).search(data[index:])
        if m:
            ifs = If(m.group(1))
            lopen.insert(0, ifs)
            index += m.end()
            continue

        m = re.compile(r'^\s*upstream\s*(.*?\S+)\s*{', re.S).search(data[index:])
        if m:
            u = Upstream(m.group(1))
            lopen.insert(0, u)
            index += m.end()
            continue

        m = re.compile(r'^\s*geo\s*(.*?\S+)\s*{', re.S).search(data[index:])
        if m:
            g = Geo(m.group(1))
            lopen.insert(0, g)
            index += m.end()
            continue

        m = re.compile(r'^\s*map\s*(.*?\S+)\s*{', re.S).search(data[index:])
        if m:
            g = Map(m.group(1))
            lopen.insert(0, g)
            index += m.end()
            continue

        m = re.compile(r'^(\s*)#\s*(.*?)\n', re.S).search(data[index:])
        if m:
            c = Comment(m.group(2), inline='\n' not in m.group(1))
            if lopen and isinstance(lopen[0], Container):
                lopen[0].add(c)
            else:
                f.add(c) if conf else f.append(c)
            index += m.end() - 1
            continue

        m = re.compile(r'^\s*}', re.S).search(data[index:])
        if m:
            if isinstance(lopen[0], Container):
                c = lopen[0]
                lopen.pop(0)
                if lopen and isinstance(lopen[0], Container):
                    lopen[0].add(c)
                else:
                    f.add(c) if conf else f.append(c)
            index += m.end()
            continue

        s1 = r'("[^"]+"|\'[^\']+\'|[^\s;]+)'
        s2 = r'("[^"]*"|\'[^\']*\'|[^\s;]*)'
        s3 = r'(\s*[^;]*?)'
        key = r'^\s*{}\s*{}{};'.format(s1, s2, s3)
        m = re.compile(key, re.S).search(data[index:])
        if m:
            k = Key(m.group(1), m.group(2) + m.group(3))
            if lopen and isinstance(lopen[0], (Container, Server)):
                lopen[0].add(k)
            else:
                f.add(k) if conf else f.append(k)
            index += m.end()
            continue

        m = re.compile(r'^\s*(\S+);', re.S).search(data[index:])
        if m:
            k = Key(m.group(1), '')
            if lopen and isinstance(lopen[0], (Container, Server)):
                lopen[0].add(k)
            else:
                f.add(k) if conf else f.append(k)
            index += m.end()
            continue

        break

    return f


def load(fobj):
    """
    Load an nginx configuration from a provided file-like object.

    :param obj fobj: nginx configuration
    """
    return loads(fobj.read())


def loadf(path):
    """
    Load an nginx configuration from a provided file path.

    :param file path: path to nginx configuration on disk
    """
    with open(path, 'r') as f:
        return load(f)


def dumps(obj):
    """
    Dump an nginx configuration to a string.

    :param obj obj: nginx object (Conf, Server, Container)
    :returns: nginx configuration as string
    """
    return ''.join(obj.as_strings)


def dump(obj, fobj):
    """
    Write an nginx configuration to a file-like object.

    :param obj obj: nginx object (Conf, Server, Container)
    :param obj fobj: file-like object to write to
    :returns: file-like object that was written to
    """
    fobj.write(dumps(obj))
    return fobj


def dumpf(obj, path):
    """
    Write an nginx configuration to file.

    :param obj obj: nginx object (Conf, Server, Container)
    :param str path: path to nginx configuration on disk
    :returns: path the configuration was written to
    """
    with open(path, 'w') as f:
        dump(obj, f)
    return path

'''
python-nginx end
'''

'''
Find all included config files from nginx.conf
'''
def find_nginx_configs (nginxconf='/etc/nginx/nginx.conf'):
    include_configs = []

    try:
        with open(nginxconf) as f:
            for line in f:
                try:
                    if line.strip().startswith('include'):
                        filenames = glob.glob(line.strip().split()[1].split(';')[0])
                        for filename in filenames:
                            if filename.endswith('.conf'):
                                include_configs.append(filename)
                except IndexError:
                    pass
    except IOError:
        pass

    return include_configs

def list_nginx_sites (nginx_config_files=''):
    output = ''

    for nginx_config_file in nginx_config_files:
        n = loadf(nginx_config_file)

        for s in n.servers:
            vhost = ''
            port = 80
            protocol = 'http'

            for k in s.keys:
                # {'listen': '*:443 ssl'}
                # {'server_name': 'bots.comingsoon.no'}
                # {'ssl': 'on'}

                if 'server_name' in k.as_dict:
                    vhost = k.as_dict['server_name'].split()[0]
                    if '_' == vhost:
                        vhost = socket.getfqdn(socket.gethostname())
                elif 'listen' in k.as_dict:
                    # i.e.
                    # listen *:80 default_server; gives 80
                    # listen 193.58.250.175:443 ssl; gives 193.58.250.175:443
                    if ':' in k.as_dict['listen']:
                        port = int(filter(str.isdigit, k.as_dict['listen'].split(':')[1]))
                    else:
                        port = int(filter(str.isdigit, k.as_dict['listen']))
                elif 'ssl' in k.as_dict:
                    protocol = 'https'

            if 'localhost' == vhost:  # Skip vhost
                continue

            output += "%s://%s:%s/ 200\n" % (protocol, vhost, port)

    return output


def list_apache_sites ():
    output = ''

    proc = subprocess.Popen("/usr/sbin/apachectl -S 2> /dev/null", shell=True, 
        stdout=subprocess.PIPE, bufsize=1, universal_newlines=True)

    for line in iter(proc.stdout.readline, b''):
        fields = line.strip().split()
        if '.conf' in line:
            protocol = 'http://'

            if 'default' == fields[0]:
                vhost = fields[2]
            elif fields[1].isdigit():
                vhost = fields[3]
            else:
                vhost = fields[1]

            if '443' == fields[1]:
                protocol = 'https://'

            # Skip creating duplicates in config file
            exists = False
            for o in output.splitlines():
                if vhost in o:
                    exists = True
                    break

            if not exists:
                output += "%s%s/ 200\n" % (protocol, vhost)

    return output


def list_configured_sites (conf):
    out = ''
    for site in conf:

        # Make string and status optional
        string = ''
        status = 200
        try:
            string = site['string']
        except KeyError:
            pass
        try:
            status = site['status']
        except KeyError:
            pass

        out += ('%s %s %s\n' % (
            site['url'],
            status,
            string
            ))
    return out


def create_file (path, content=''):
    print("writing file %s with %s" % (path, content))
    try:
        with open(path, 'w') as f:
            f.write(content)
    except IOError as err:
        print ("Unable to create file %s\n(%s)." % (path, err))
        sys.exit(1)


# def import_config (cfg):
#     sys.path.append(os.path.dirname(os.path.expanduser(
#         cfg)))
#     try:
#         import webchecksconfig
#     except ImportError:
#         # No config file exists, nothing specified
#         create_file (path=cfg)
#     except SyntaxError as err:
#         print("Error reading config %s: %s" % (cfg, err))
#         sys.exit(1)


def skeletton_config ():
    config = '''
# -*- encoding: utf-8; py-indent-offset: 4 -*-

 # Disable auto detection of vhosts from web servers
disable = {
    'apache': False,
    'nginx': False,
}

sites = [
    # {
    #     'url': 'https://example.com/',
    #     'status': 200                                          # optional
    #     'string': 'string to look for. One or several words.', # optional
    # },  # If https, ssl check will be performed and return number of days left
    '''
    found_sites = ''
    found_sites += list_nginx_sites(find_nginx_configs())
    found_sites += list_apache_sites()

    for site in found_sites.splitlines():
        config += '''    {
        'url': '%s',
        'status': 200,
    },
''' % site.split()[0]

    config += "\n]\n"

    return config


def main():
    global config_file
    disable_nginx = False
    disable_apache = False
    output = "<<<webchecks>>>\n"

    # Parse arguments from user
    parser = argparse.ArgumentParser(
        description=('''
        Agent for Check_mk WebChecks plugin. See
        https://gitlab.com/larsfp/checkmk-WebChecks
        for help.'''))
    parser.add_argument('-w', '--write', action="store_true",
        help='Update config file %s, even if it exists.' % config_file)
    parser.add_argument('-c', '--config',
        help='Use specified config file instead of default %s.' % config_file)
    args = parser.parse_args()

    # Use specified config file
    if args.config:
        # If none, initialize
        #if not os.path.isfile(args.config):
        #    create_file (path=args.config, content=skeletton_config())
        config_file = args.config

    # TODO: Update config
    if args.write:
        print (args.write)
        print ("TODO")

    # Import config file
    sys.path.append(os.path.dirname(os.path.expanduser(
        config_file)))
    try:
        import webchecksconfig
    except ImportError:
        # No config file exists, nothing specified
        create_file (path=config_file, content=skeletton_config())
        import webchecksconfig
    except SyntaxError as err:
        print("Error reading config %s: %s" % (config_file, err))
        sys.exit(1)

    # Add configured sites
    try:
        output += list_configured_sites (webchecksconfig.sites)
#    except NameError:  # Handle missing config
#        pass
    except AttributeError:
        exit("Please update your config file to latest version.")

    # Add sites from nginx
    try:
        disable_nginx = webchecksconfig.disable['nginx']
#    except NameError:  # Handle missing config
#        pass
    except AttributeError:
        pass
    except KeyError:
        pass

    if not disable_nginx:
        output += list_nginx_sites(find_nginx_configs())
  
    # Add sites from apache
    try:
        disable_apache = webchecksconfig.disable['apache']
    except NameError:  # Handle missing config
        pass
    except AttributeError:
        pass
    except KeyError:
        pass

    if not disable_apache:
        output += list_apache_sites()

    if 17 < len(output): # Only print if we have more that header
        print(output)


if __name__ == "__main__":
    main()
